CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS users(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  email varchar(50) NOT NULL,
  password CHAR(60) NOT NULL,
  firstname varchar(50),
  lastname varchar(50),
  email_verified SMALLINT DEFAULT 0
);
