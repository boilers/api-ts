# api-ts
Typescript API.

Running this project requires docker and docker-compose.

## Getting started
Clone the repository.
```bash
git clone https://gitlab.com/boilers/api-ts.git
cd api-ts
```
Copy example.env.
```bash
cp example.env .env
```
Install dependencies and build the docker image.
```bash
npm install
docker build .
```
Start the development environment.
```bash
docker-compose up
```
