import { Request, Response, NextFunction } from 'express'
import pg from '../postgres'
import Session from '../session'
import bcrypt from 'bcryptjs'
import { uuid } from '../utils/index'
import * as regexp from '../utils/regexp'

const salt = bcrypt.genSaltSync(10)

import {
  missingRequired,
  missingOptional,
  internalServerError,
  UNAUTHORIZED,
  EMAIL_EXISTS,
  PASSWORDS_NO_MATCH,
  EMAIL_INVALID,
} from '../utils/responses'

const cookieConfig = {
  httpOnly: true,
}

export default class User {
  static async signUp (req: Request, res: Response) {
    try {
      const { email, password, passwordRepeat } = req.body
      const ERROR = missingRequired({ email, password, passwordRepeat })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const emailIsValid = regexp.email.test(email)
      if (!emailIsValid) {
        return res.status(EMAIL_INVALID.code).json(EMAIL_INVALID)
      }

      if (password !== passwordRepeat) {
        return res.status(PASSWORDS_NO_MATCH.code).json(PASSWORDS_NO_MATCH)
      }

      const emailQuery: string = 'SELECT * FROM users WHERE email = $1'
      // @ts-ignore
      const result = await pg.query(emailQuery, [email])
      const [emailExists] = result.rows
      if (emailExists) {
        return res.status(EMAIL_EXISTS.code).json(EMAIL_EXISTS)
      }

      const passwordHashed = await bcrypt.hashSync(password, salt)

      const insertQuery = 'INSERT INTO users(email, password) VALUES ($1, $2)'
      // @ts-ignore
      await pg.query(insertQuery, [email, passwordHashed])

      User.signIn(req, res)
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async signIn (req: Request, res: Response) {
    try {
      const { email, password } = req.body
      const ERROR = missingRequired({ email, password })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const query: string = 'SELECT * FROM users WHERE email = $1'
      // @ts-ignore
      const result = await pg.query(query, [email])
      let [user] = result.rows
      if (!user) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const passwordMatch = await bcrypt.compareSync(password, user.password)
      if (!passwordMatch) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const token = uuid()
      const session = { id: user.id }
      await Session.set(token, session)
      res.cookie('token', token, cookieConfig)

      user = User.sanitize(user)
      res.json(user)
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async signOut (req: Request, res: Response) {
    try {
      const { token } = req.cookies
      if (token) {
        await Session.del(token)
        res.clearCookie('token')
      }

      res.status(201).end()
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async authenticate (req: Request, res: Response, next: NextFunction) {
    try {
      const { token } = req.cookies
      const session = await Session.get(token)
      if (session) {
        req.me = session
      }

      next()
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async update (req: Request, res: Response) {
    try {
      const { one, two } = req.body
      const ERROR = missingOptional({ one, two })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      res.json({
        code: 200,
      })
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async updateEmail (req: Request, res: Response) {
    try {
      const { me } = req
      if (!me) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const { email, password } = req.body
      const ERROR = missingRequired({ email, password })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const emailIsValid = regexp.email.test(email)
      if (!emailIsValid) {
        return res.status(EMAIL_INVALID.code).json(EMAIL_INVALID)
      }

      const emailQuery: string = 'SELECT email FROM users WHERE email = $1'
      // @ts-ignore
      const emailResult = await pg.query(emailQuery, [email])
      const [emailExists] = emailResult.rows
      if (emailExists) {
        return res.status(EMAIL_EXISTS.code).json(EMAIL_EXISTS)
      }

      const userQuery: string = 'SELECT password FROM users WHERE id = $1'
      // @ts-ignore
      const userResult = await pg.query(userQuery, [me.id])
      const [user] = userResult.rows
      const passwordMatch = await bcrypt.compareSync(password, user.password)
      if (!passwordMatch) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const updateQuery: string = 'UPDATE users SET email = $2, email_verified = 0 WHERE id = $1 RETURNING email'
      // @ts-ignore
      const updateResult = await pg.query(updateQuery, [me.id, email])
      let [updated] = updateResult.rows

      updated = User.sanitize(updated)
      res.json(updated)
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async verifyEmail (req: Request, res: Response) {
    try {
      res.end()
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static async updatePassword (req: Request, res: Response) {
    try {
      const { me } = req
      if (!me) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const { password, newPassword, newPasswordRepeat } = req.body
      const ERROR = missingRequired({ password, newPassword, newPasswordRepeat })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      if (newPassword !== newPasswordRepeat) {
        return res.status(PASSWORDS_NO_MATCH.code).json(PASSWORDS_NO_MATCH)
      }

      const userQuery: string = 'SELECT password FROM users WHERE id = $1'
      // @ts-ignore
      const result = await pg.query(userQuery, [me.id])
      const [user] = result.rows
      const passwordMatch = await bcrypt.compareSync(password, user.password)
      if (!passwordMatch) {
        return res.status(UNAUTHORIZED.code).json(UNAUTHORIZED)
      }

      const newPasswordHashed = await bcrypt.hashSync(newPassword, salt)
      const updateQuery: string = 'UPDATE users SET password = $2 WHERE id = $1'
      // @ts-ignore
      await pg.query(updateQuery, [me.id, newPasswordHashed])

      res.status(201).end()
    } catch (error) {
      internalServerError(req, res, error)
    }
  }

  static sanitize (user: { [index: string]: any }) {
    delete user.password

    return user
  }
}
