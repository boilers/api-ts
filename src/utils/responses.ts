import { Request, Response } from 'express'
import log from './logger'

interface ERROR {
  code: number
  message: string
}

function missingParameters (
  params: { [index: string]: any },
  message: string,
  optional = false,
): ERROR | null {
  const missing: string[] = []
  for (const key in params) {
    if (params[key] === undefined) missing.push(key)
  }

  if (optional && missing.length !== Object.keys(params).length) return null
  if (!missing.length) return null

  return {
    code: 400,
    message: `${message}: ${missing.join(', ')}.`
  }
}

export function missingRequired (params: { [index: string]: any }): ERROR | null {
 return missingParameters(params, 'Missing required parameter(s)')
}
export function missingOptional (params: { [index: string]: any }): ERROR | null {
 return missingParameters(params, 'Missing optional parameter(s)', true)
}

export const INTERNAL_SERVER_ERROR: ERROR = {
  code: 500,
  message: 'Internal server error.'
}
export function internalServerError (req: Request, res: Response, error: Error) {
  log(error)
  res.status(INTERNAL_SERVER_ERROR.code).json(INTERNAL_SERVER_ERROR)
}

export const UNAUTHORIZED: ERROR = {
  code: 401,
  message: 'Unauthorized request.'
}

export const EMAIL_EXISTS: ERROR = {
  code: 422,
  message: 'E-mail is already registered.',
}

export const PASSWORDS_NO_MATCH: ERROR = {
  code: 422,
  message: 'Passwords do not match.',
}

export const EMAIL_INVALID: ERROR = {
  code: 400,
  message: 'Invalid e-mail.',
}
