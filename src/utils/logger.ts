import { Request, Response, NextFunction } from 'express'
import { typeOf } from './index'

const { NODE_ENV } = process.env
const isDevelopment = NODE_ENV === 'development'

const blacklistKeys: string[] = [
  // PERSONAL INFO
  'ssn', // TODO: use regexp instead

  // PASSWORDS
  'password',
  'passwordRepeat',
  'newPassword',
  'newPasswordRepeat',
]

import * as regexp from './regexp'
const blacklistRegexp: RegExp[] = [
  regexp.email,
]

function sanitizeSensitiveString (input: string): string {
  let result = input
  for (const r of blacklistRegexp) {
    if (r.test(input)) {
      result = '** REDACTED **' // TODO: use string.replace instead
      break
    }
  }
  return result
}

function sanitizeSensitiveField (data: any): any {
  const type: string = typeOf(data)
  let value: any = data

  switch (type) {
    case 'number':
    case 'boolean':
      value = data
      break
    case 'string':
      value = sanitizeSensitiveString(data)
      break
  }
  return value
}

function sanitizeSensitiveData (data: any): any {
  let sanitized: any
  const type: string = typeOf(data)

  switch (type) {
    case 'object':
      sanitized = {}

      for (const key in data) {
        const t = typeOf(data[key])
        if (t === 'object' || t === 'array') {
          sanitized[key] = sanitizeSensitiveData(data[key])
          continue
        }

        if (blacklistKeys.includes(key)) {
          sanitized[key] = '** REDACTED **'
          continue
        }

        sanitized[key] = sanitizeSensitiveField(data[key])
      }
      break

    case 'array':
      sanitized = []

      for (const index in data) {
        const t = typeOf(data[index])
        if (t === 'object' || t === 'array') {
          sanitized.push(sanitizeSensitiveData(data[index]))
          continue
        }

        sanitized.push(sanitizeSensitiveField(data[index]))
      }
      break

    default:
      break
  }

  return sanitized
}

export function logRequest (options?: { [index: string]: any }) {
  return function (req: Request, res: Response, next: NextFunction): void {
    console.log('\n' + new Date())
    const { url, method } = req

    if (isDevelopment) {
      console.log(`${method}: ${url}`)

      const m = method.toLowerCase()
      if (m === 'post' || m === 'put') {
        const sanitized = sanitizeSensitiveData(req.body)
        console.log(JSON.stringify(sanitized, null, 2))
      }
    } else {
      // TODO: production logs
    }

    next()
  }
}

export default function log (...args: any[]): void {
  if (isDevelopment) {
    // TODO: sanitizeSensitiveData
    console.log(...args)
  } else {
    // TODO: production logs
  }
}
