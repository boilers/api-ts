import { Router, Request, Response } from 'express'
const router: Router = Router()
import User from '../controllers/user'

router.post('/sign-up', User.signUp)
router.post('/sign-in', User.signIn)
router.get('/sign-out', User.signOut)
router.get('/authenticate', User.authenticate, (req: Request, res: Response) => {
  const { me } = req
  res.json(me)
})
router.put('/update', User.authenticate, User.update)
router.put('/update-email', User.authenticate, User.updateEmail)
router.put('/update-password', User.authenticate, User.updatePassword)

export default router
