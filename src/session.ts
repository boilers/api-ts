import redis from './redis'

export interface SessionObject {
  id: string
}

export default class Session {
  static async set (token: string, data: SessionObject): Promise<string> {
    const session = JSON.stringify(data)
    return redis.set(token, session)
  }

  static async get (token: string): Promise<SessionObject | null> {
    const session: string | null = await redis.get(token)
    return JSON.parse(session as string)
  }

  static async del (token: string): Promise<number> {
    return redis.del(token)
  }
}
