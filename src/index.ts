import express, { Application } from 'express'
const app: Application = express()

import './redis'
import './postgres'

// TODO: setup cors so httpOnly cookie works

import cookieParser from 'cookie-parser'
app.use(cookieParser())

import bodyParser from 'body-parser'
app.use(bodyParser.json())

import log, { logRequest } from './utils/logger'
app.use(logRequest())

import router from './routes/index'
app.use(router)

const PORT: number = parseInt(process.env.PORT as string) || 3000
const HOST: string = process.env.HOST || '0.0.0.0'
app.listen(PORT, HOST, () => {
  log(`API running on http://${HOST}:${PORT}`)
})
