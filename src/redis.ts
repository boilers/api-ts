import Redis from 'ioredis'
import log from './utils/logger'

const REDIS_PORT: number = parseInt(process.env.REDIS_PORT as string)
const REDIS_HOST: string = process.env.REDIS_HOST as string
const options = {
  port: REDIS_PORT,
  host: REDIS_HOST,
}

const redis = new Redis(options)
redis.on('connect', () => {
  log('Connected to Redis.')
})
redis.on('error', (error) => {
  log('Could not connect to Redis.')
  log(error)
})

export default redis
