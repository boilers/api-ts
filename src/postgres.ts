import { Pool } from 'pg'
import log from './utils/logger'

const user: string = process.env.PG_USER as string
const password: string = process.env.PG_PASSWORD as string
const port: number = parseInt(process.env.PG_PORT as string)
const database: string = process.env.PG_DATABASE as string
const host: string = process.env.PG_HOST as string

const pool = new Pool({
  user,
  password,
  host,
  port,
  database,
})

pool.connect()
  .then(response => log('Connected to PostgreSQL.'))
  .catch(error => log(error))

export default {
  query (text: string, values: []) {
    return pool.query(text, values)
  }
}

